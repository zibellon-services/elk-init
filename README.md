# LogsControl STACK стек

## Основные ссылки на все, что было использовано

1. https://hub.docker.com/r/timberio/vector - docker image Vector
2. https://habr.com/ru/company/badoo/blog/507718/ - статья про LOKI
3. https://hub.docker.com/r/grafana/loki - docker image loki
4. https://hub.docker.com/r/grafana/grafana - docker image grafana
5. https://vector.dev/docs/ - Документация по Vector
6. https://grafana.com/docs/loki/latest/ - документация по LOKI
7.  https://grafana.com/docs/grafana/latest/ - документация по Grafana

## Основные составные части сервиса

- Grafana LOKI
- Grafana
- Vector
  - Master
  - Slave

## Порядок запуска

### Запуск Loki

```
./init.sh loki
```

### Запуск Grafana

```
./init.sh grafana
```

### Запуск VectorSlave (на каждом отдельном сервере)

```
./init.sh vector-slave
```

### Запуск VectorMaster (Только на головном сервере)

```
./init.sh vector-master
```

## Полная остановка всех контейнеров

```
./init.sh stop-all
```

## Полная остановка всех контейнеров + очистка системы (Удаление всех файлов)

```
./init.sh stop-and-clear
```

## Интересный ссылки
1. https://habr.com/ru/post/489924/ - Интересная статья на HABR
2. 