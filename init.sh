#!/bin/bash

#Запуск loki
function loki () {
    echo "start loki"
    
    echo "createLokiFolder"
    [ ! -d "data-loki" ] && mkdir data-loki && chmod -R 777 ./data-loki && echo "Directory created."
    
    FILE=dc-loki.yaml
    
    echo "DOWN loki docker container"
    docker-compose -f $FILE down
    
    echo "UP loki docker container"
    docker-compose -f $FILE up -d
    
    echo "LOGS loki"
    docker-compose -f $FILE logs -f
}

#Запуск grafana
function grafana () {
    echo "start grafana"
    
    echo "createGrafanaFolder"
    [ ! -d "data-grafana" ] && mkdir data-grafana && chmod -R 777 ./data-grafana && echo "Directory created."
    
    FILE=dc-grafana.yaml
    
    echo "DOWN grafana docker container"
    docker-compose -f $FILE down
    
    echo "UP grafana docker container"
    docker-compose -f $FILE up -d
    
    echo "LOGS grafana"
    docker-compose -f $FILE logs -f
}

#Запуск VectorMaster
function vectorMaster () {
    echo "start vectorMaster"
    
    FILE=dc-vector-master.yaml
    
    echo "DOWN vectorMaster docker container"
    docker-compose -f $FILE down
    
    echo "UP vectorMaster docker container"
    docker-compose -f $FILE up -d
    
    echo "LOGS vectorMaster"
    docker-compose -f $FILE logs -f
}

#Запуск VectorSlave
function vectorSlave () {
    echo "start vectorSlave"
    
    FILE=dc-vector-slave.yaml
    
    echo "DOWN vectorSlave docker container"
    docker-compose -f $FILE down
    
    echo "UP vectorSlave docker container"
    docker-compose -f $FILE up -d
    
    echo "LOGS vectorSlave"
    docker-compose -f $FILE logs -f
}

# Остановка всех контейнеров
function stopAll () {
    echo "stopAll"
    docker-compose -f dc-loki.yaml down
    docker-compose -f dc-grafana.yaml down
    docker-compose -f dc-vector-master.yaml down
    docker-compose -f dc-vector-slave.yaml down
}

# Остановка всех контейнеров
function stopAndClear () {
    echo "stopAndClear"
    stopAll
    
    echo "remove all folders with data files"
    rm -Rf ./data-grafana
    rm -Rf ./data-loki
}

#################################

# $1 - CommandName
# $2 - argument

case $1 in
    loki)
        loki
    ;;
    grafana)
        grafana
    ;;
    vector-master)
        vectorMaster
    ;;
    vector-slave)
        vectorSlave
    ;;
    stop-all)
        stopAll
    ;;
    stop-and-clear)
        stopAndClear
    ;;
    *)
        echo "Invalid option, choose again..."
        exit 1
    ;;
esac